# RentITUI

## Describtion
This project is the frontend part of an API that allow to rent IT Articles and return IT Articles. 

Features:
 - Admin:
   - CRUD operations on Articles

 - User:
   - Rent and return articles (listed in a personal dashboard)
   - Extend the return Date of rented articles


## Backtend
The backend is an Java Spring boot project.

Repos Link: https://gitlab.com/Baudouinken/rentit-api


## Deploy
The App is doplyed on a linux root-server with gitlab-runners through a [gitlab.yml](https://gitlab.com/Baudouinken/rentit-ui/-/blob/card/.gitlab-ci.yml) file
