export class SignupRequest {
    public firstname: string;
    public lastname: string;
    public street: string;
    public houseNumber: string;
    public city: string;
    public plz: string;
    public email: string;
    public password: string;

}