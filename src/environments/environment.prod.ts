export const environment = {
  prod: true,
  env: 'Dev',
  appName: 'Rentit-UI',
  version: '0.0.1',
  apiUrl: "http://54.175.46.235:8000/api"
};
